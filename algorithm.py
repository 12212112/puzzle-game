# -*- coding: utf-8 -*-
# @Time    : 2024/3/3
# @Author  : Shishen Xian
# @mail    : shishen_xian@sjtu.edu.cn

import numpy as np
import base 
from base import operate
import tqdm
import copy
import argparse
import time


def print_operate(module :str,  module_para_inst):
    print(module,"| ","rot:", module_para_inst[0], "mirror:", module_para_inst[1], "x:",module_para_inst[2],"y:",module_para_inst[3] )


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description = "Todo")
    parser.add_argument("--month", default="MAR", help="JAN, FEB, MAR, APR, MAY, JUN,JUL, AUG, SEP, OCT, NOV, DEC")
    parser.add_argument("--day", default=1, help="1-31")
    args = parser.parse_args()

    start_time = time.time()

    day_string = [" 1 ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 "," 8 ", " 9 ", "10 ", "11 ", "12 ", "13 ", "14 ","15 ", "16 ", "17 ", "18 ", "19 ", "20 ", "21 ","22 ", "23 ", "24 ", "25 ", "26 ", "27 ", "28 ","29 ", "30 ", "31 "]
    month_string = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN","JUL", "AUG", "SEP", "OCT", "NOV", "DEC"]
    
    if args.month not in month_string:
        raise ValueError(f"'{args.month}' is not a valid month string.")
    else:
        month = args.month
    
    day_num = int(args.day)
    day = day_string[day_num - 1]
    
    #paramter space
    rotate_operate_all = np.array([0,1,2,3])
    rotate_operate_half = np.array([0,1])
    mirror_operate_all = np.array([0,1])
    mirror_operate_half = np.array([0])
    x_range_all = np.array([0,1,2,3,4,5])
    y_range_all = np.array([0,1,2,3,4,5])
    x_range_small = np.array([0,1,2,3,4])
    y_range_small = np.array([0,1,2,3,4])

    #initilize all mudule
    screen_parmeter = []
    for r in rotate_operate_half:
        for m in mirror_operate_half:
            for x in x_range_all:
                for y in y_range_all:
                    board = base.Board(month, day)
                    screen = base.Module_SCREEN()
                    operate(screen,r,m)
                    if (board.place_modile(screen.get_shape(),x,y) == 0):
                        screen_parmeter.append(np.array([r,m,x,y]))
    trap_parmeter = []

    for r in rotate_operate_all:
        for m in mirror_operate_half:
            for x in x_range_all:
                for y in y_range_all:
                    board = base.Board(month, day)
                    trap  = base.Module_TRAP()
                    operate(trap,r,m)
                    if (board.place_modile(trap.get_shape(),x,y) == 0):
                        trap_parmeter.append(np.array([r,m,x,y]))

    crutch_parmeter = []
    for r in rotate_operate_all:
        for m in mirror_operate_all:
            for x in x_range_small:
                for y in y_range_all:
                    board = base.Board(month, day)
                    crutch = base.Module_CRUTCH()
                    operate(crutch,r,m)
                    if (board.place_modile(crutch.get_shape(),x,y) == 0):
                        crutch_parmeter.append(np.array([r,m,x,y]))

    z_parmeter = []
    for r in rotate_operate_half:
        for m in mirror_operate_all:
            for x in x_range_small:
                for y in y_range_small:
                    board = base.Board(month, day)
                    z = base.Module_Z()
                    operate(z,r,m)
                    if (board.place_modile(z.get_shape(),x,y) == 0):
                        z_parmeter.append(np.array([r,m,x,y]))

    chair_parmeter = []
    for r in rotate_operate_all:
        for m in mirror_operate_all:
            for x in x_range_all:
                for y in y_range_all:
                    board = base.Board(month, day)
                    chair = base.Module_CHAIR()
                    operate(chair,r,m)
                    if (board.place_modile(chair.get_shape(),x,y) == 0):
                        chair_parmeter.append(np.array([r,m,x,y]))          

    snake_parmeter = []
    for r in rotate_operate_all:
        for m in mirror_operate_all:
            for x in x_range_all:
                for y in y_range_all:
                    board = base.Board(month, day)
                    snake = base.Module_SNAKE()
                    operate(snake,r,m)
                    if (board.place_modile(snake.get_shape(),x,y) == 0):
                        snake_parmeter.append(np.array([r,m,x,y]))

    l_parmeter = []
    for r in rotate_operate_all:
        for m in mirror_operate_all:
            for x in x_range_small:
                for y in y_range_all:
                    board = base.Board(month, day)
                    l = base.Module_L()
                    operate(l,r,m)
                    if (board.place_modile(l.get_shape(),x,y) == 0):
                        l_parmeter.append(np.array([r,m,x,y]))

    boomerang_parmeter = []
    for r in rotate_operate_all:
        for m in mirror_operate_half:
        # for m in mirror_operate_all:
            for x in x_range_small:
                for y in y_range_small:
                    board = base.Board(month, day)
                    boomerang = base.Module_BOOMERANG()
                    operate(boomerang,r,m)
                    if (board.place_modile(boomerang.get_shape(),x,y) == 0):
                        boomerang_parmeter.append(np.array([r,m,x,y]))
    
    #search the solutuon
    cnt = 0
    for screen_inst in tqdm.tqdm(screen_parmeter):
        board = base.Board(month, day)
        screen = base.Module_SCREEN()
        operate(screen,screen_inst[0],screen_inst[1])
        board.place_modile(screen.get_shape(),screen_inst[2],screen_inst[3])

        copied_board_1 = copy.deepcopy(board)
        # backup the board 
        for trap_inst in trap_parmeter:
            trap  = base.Module_TRAP()
            operate(trap,trap_inst[0],trap_inst[1])
            if (board.place_modile(trap.get_shape(),trap_inst[2],trap_inst[3]) == 0):
                copied_board_2 = copy.deepcopy(board)
                # backup the board 
                for crutch_inst in crutch_parmeter:
                    crutch = base.Module_CRUTCH()
                    operate(crutch,crutch_inst[0],crutch_inst[1])
                    if (board.place_modile(crutch.get_shape(),crutch_inst[2],crutch_inst[3]) == 0):
                        copied_board_3 = copy.deepcopy(board)
                        # backup the board 
                        for z_inst in z_parmeter:                 
                            z = base.Module_Z()
                            operate(z,z_inst[0],z_inst[1])
                            if(board.place_modile(z.get_shape(), z_inst[2], z_inst[3]) == 0):
                                copied_board_4 = copy.deepcopy(board)
                                # backup the board 
                                for chair_inst in chair_parmeter:
                                    chair = base.Module_CHAIR()
                                    operate(chair,chair_inst[0],chair_inst[1])
                                    if (board.place_modile(chair.get_shape(), chair_inst[2],chair_inst[3]) == 0):
                                        copied_board_5 = copy.deepcopy(board)
                                        # backup the board 
                                        for snake_inst in snake_parmeter:
                                            snake = base.Module_SNAKE()
                                            operate(snake,snake_inst[0],snake_inst[1])
                                            if (board.place_modile(snake.get_shape(),snake_inst[2],snake_inst[3]) == 0):  
                                                copied_board_6 = copy.deepcopy(board)
                                                # backup the board 
                                                for l_inst in l_parmeter:
                                                    l = base.Module_L()
                                                    operate(l,l_inst[0],l_inst[1])
                                                    if (board.place_modile(l.get_shape(),l_inst[2],l_inst[3]) == 0):
                                                        copied_board_7 = copy.deepcopy(board)
                                                        # backup the board 
                                                        for boomerang_inst in boomerang_parmeter:
                                                            boomerang = base.Module_BOOMERANG()
                                                            operate(boomerang,boomerang_inst[0],boomerang_inst[1])
                                                            if (board.place_modile(boomerang.get_shape(),boomerang_inst[2],boomerang_inst[3]) == 0):
                                                                print("######## one solution ############")
                                                                print_operate(" screen  ", screen_inst)
                                                                print_operate("  trap   ", trap_inst)
                                                                print_operate(" crutch  ", crutch_inst)
                                                                print_operate("    z    ", z_inst)
                                                                print_operate("  chair  ", chair_inst)
                                                                print_operate("  snake  ", snake_inst)
                                                                print_operate("    l    ", l_inst)
                                                                print_operate("boomerang", boomerang_inst)
                                                                cnt += 1                         
                                                                # continue
                                                            else:
                                                                board = copy.deepcopy(copied_board_7)
                                                                continue
                                                    else:
                                                        board = copy.deepcopy(copied_board_6)   
                                                        continue         
                                            else:
                                                board = copy.deepcopy(copied_board_5)
                                                continue
                                    else:                                    
                                        board = copy.deepcopy(copied_board_4)
                                        continue
                            else:
                                board = copy.deepcopy(copied_board_3)                   
                                continue             
                    else:
                        board = copy.deepcopy(copied_board_2) 
                        continue
            else:
                board = copy.deepcopy(copied_board_1)
                continue


    print("find", cnt, "solutions")

    end_time = time.time()
    execution_time = end_time - start_time
    print(f"code excute with {execution_time} s")
