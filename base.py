# -*- coding: utf-8 -*-
# @Time    : 2024/3/3
# @Author  : Shishen Xian
# @mail    : shishen_xian@sjtu.edu.cn

import numpy as np
from scipy.signal import convolve2d

def count_surrounded_by_X(matrix, kernel):
    # Apply convolution
    surrounded_count = convolve2d(matrix, kernel, mode='valid')
    return surrounded_count

def check_1(array):
    # Create a boolean mask for each value
    mask_40 = array == 40
    mask_41 = array == 41
    mask_42 = array == 42
    mask_43 = array == 43
    mask_44 = array == 44
    
    # Check if all values exist in the array
    return mask_40.any() or mask_41.any() or mask_42.any() or mask_43.any() or mask_44.any()

def check_2(array):
    # Create a boolean mask for each value
    mask_120 = array == 120
    mask_121 = array == 121
    mask_122 = array == 122
    mask_123 = array == 123
    mask_124 = array == 124
    
    # Check if all values exist in the array
    return mask_120.any() or mask_121.any() or mask_121.any() or mask_122.any() or mask_123.any()

class Board:
    def __init__(self, month, day) -> None:
        calendar = [["JAN", "FEB", "MAR", "APR", "MAY", "JUN"],
                      ["JUL", "AUG", "SEP", "OCT", "NOV", "DEC"],
                      [" 1 ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 "],
                      [" 8 ", " 9 ", "10 ", "11 ", "12 ", "13 ", "14 "],
                      ["15 ", "16 ", "17 ", "18 ", "19 ", "20 ", "21 "],
                      ["22 ", "23 ", "24 ", "25 ", "26 ", "27 ", "28 "],
                      ["29 ", "30 ", "31 "]]
        buffer = [row[:] for row in calendar]
        for i in range(len(calendar)):
            for j in range(len(calendar[i])):
                if buffer[i][j] in [month, day]:
                    buffer[i][j] = " X "
        self.board = buffer
    
    def print_board(self):
        for row in self.board:
            print(row)
    
    def check_opearte(self):
        kernel1 =  np.array([[1, 10, 1],
                            [10, 5, 10],
                            [1, 10, 1]])
        
        kernel2_1 = np.array([[1, 20, 1],
                            [20, 5, 20],
                            [20, 5, 20],
                            [1, 20, 1]])
        kernel2_2 = np.array([[1, 20, 20, 1],
                            [20, 5, 5, 20],
                            [1, 20, 20, 1]])
                    
        padded_board = [[' X '] * 9 for _ in range(9)]
        for i in range(0, len( self.board)):
            for j in range(0, len( self.board[i])):
                padded_board[i + 1 ][j + 1] =  self.board[i][j]
        
        matrix =  np.where(np.array(padded_board) == ' X ', 1, 0)
        convolve1 = count_surrounded_by_X(matrix, kernel1)

        if(check_1(convolve1) == True):
            return 1
        else:
            convolve2_1 = count_surrounded_by_X(matrix, kernel2_1)
            convolve2_2 = count_surrounded_by_X(matrix, kernel2_2)
            if ((check_2(convolve2_1) == False) and (check_2(convolve2_2) == False)):
                return 0
            else:
                return 1

    def place_modile(self, shape, row, col):
        buffer = [row[:] for row in self.board]
        for i in range(len(shape)):
            for j in range(len(shape[i])):
                if shape[i][j] == None:
                   continue
                else:
                    try:
                        if(self.board[row + i][col + j] == " X "):
                            self.board = buffer # back to inital state  
                            return 1
                        else:
                            self.board[row + i][col + j] = shape[i][j]
                    except IndexError:
                        self.board = buffer # back to inital state  
                        return 1       
        if (self.check_opearte()== 1):
            self.board = buffer
            return 1
        else :
            return 0   
         
    def check(self):

        return all(element == " X " for row in self.board for element in row)

class Module:
    def __init__(self, shape) -> None:
        self.shape = shape

    def print_shape(self):
        for row in self.shape:
            print(row)

    def get_shape(self):
        return self.shape

    def rotate_clockwise(self):
        # Get the dimensions of the shape
        rows = len(self.shape)
        cols = max(len(row) for row in self.shape)

        # Create a new empty shape with dimensions swapped for rotation
        rotated_shape = [[None] * rows for _ in range(cols)]

        # Rotate the shape
        for i in range(rows):
            for j in range(len(self.shape[i])):
                rotated_shape[j][-i - 1] = self.shape[i][j]

        # Update the shape with the rotated one
        self.shape = rotated_shape
    
    def mirror_symmetry(self):
        # Get the dimensions of the shape
        rows = len(self.shape)
        max_cols = max(len(row) for row in self.shape)

        # Create a new empty shape with mirrored dimensions
        mirrored_shape = [[None] * max_cols for _ in range(rows)]

        # Mirror the shape
        for i in range(rows):
            for j in range(len(self.shape[i])):
                mirrored_shape[i][max_cols - j - 1] = self.shape[i][j]

        # Update the shape with the mirrored one
        self.shape = mirrored_shape

class Module_Z(Module):
    def __init__(self) -> None:
        shape = [[" X "," X ",None],
                 [None," X ", None],
                 [None," X "," X "]]
        super().__init__(shape)

class Module_L(Module):
    def __init__(self) -> None:
        shape = [[" X ", None],
                 [" X ", None],
                 [" X ", None],
                 [" X "," X "]]
        super().__init__(shape)

class Module_SNAKE(Module):
    def __init__(self) -> None:
        shape = [[" X "," X ", None, None],
                 [None," X "," X "," X "]]
        super().__init__(shape)

class Module_TRAP(Module):
    def __init__(self) -> None:
        shape = [[" X ",None," X "],
                 [" X "," X "," X "]]
        super().__init__(shape)

class Module_BOOMERANG(Module):
    def __init__(self) -> None:
        shape = [[" X ", None, None],
                 [" X ", None, None],
                 [" X "," X "," X "]]
        super().__init__(shape)

class Module_CRUTCH(Module):
    def __init__(self) -> None:
        shape = [[" X ", None],
                 [" X "," X "],
                 [" X ", None],
                 [" X ", None]]
        super().__init__(shape)

class Module_SCREEN(Module):
    def __init__(self) -> None:
        shape = [[" X "," X "],
                 [" X "," X "],
                 [" X "," X "]]
        super().__init__(shape)

class Module_CHAIR(Module):
    def __init__(self) -> None:
        shape = [[" X ", None],
                 [" X "," X "],
                 [" X "," X "]]
        super().__init__(shape)

def operate(module_in: Module, rotate_num: int, mirror_num: int):
    for i in range(rotate_num):
        module_in.rotate_clockwise()
    for i in range(mirror_num):
        module_in.mirror_symmetry()

if __name__ == '__main__':

    Board_Instace = Board("APR", "11 ")
    Board_Instace.print_board()
    trap = Module_TRAP()
    crutch = Module_CRUTCH()
    chair = Module_CHAIR()
    z = Module_Z()
    screen = Module_SCREEN()
    boomerang = Module_BOOMERANG()
    snake = Module_SNAKE()
    l = Module_L()
    
    operate(screen,0,0)
    operate(trap,1,0)
    operate(crutch,0,1)
    operate(chair,2,0)
    operate(z,0,0)

    operate(boomerang,0,1)
    # operate(boomerang,3,0)
    operate(snake,0,1)
    operate(l,3,0)
    
    Board_Instace.place_modile(screen.get_shape(),3,0)
    Board_Instace.print_board()
    Board_Instace.place_modile(trap.get_shape(),0,0)
    Board_Instace.print_board()
    Board_Instace.place_modile(crutch.get_shape(),0,1)
    Board_Instace.print_board()
    Board_Instace.place_modile(chair.get_shape(),1,3)
    Board_Instace.print_board()
    Board_Instace.place_modile(z.get_shape(),0,4)
    Board_Instace.print_board()

    Board_Instace.place_modile(boomerang.get_shape(),4,0)
    Board_Instace.print_board()
    Board_Instace.place_modile(snake.get_shape(),3,3)
    Board_Instace.print_board()
    Board_Instace.place_modile(l.get_shape(),4,3)
    Board_Instace.print_board()
