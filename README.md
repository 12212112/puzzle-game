# Puzzle game

![1713072840322](image/README/1713072840322.png)

## Example Goal

![1713073116139](image/README/1713073116139.png)

## Python Package

scipy = 1.7.0

numpy = 1.25.0

tqdm(optional) = 4.62.3

## Code Exmaple

python algorithm.py --month SEP --day 20

## Comment

1. When place a module into board, the (x,y) is the board position that corresponds to upper left corner  of module
2. These algorthim can not find all solutuon, for exmaple, in base.py there is a exmple which can get from algorthim.
3. The time of solve one case is about 5 - 10 mins.
